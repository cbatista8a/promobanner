<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Op_promobanner extends Module
{

    protected $images_dir;

    public function __construct()
    {
        $this->name = 'op_promobanner';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'OrangePix Srl.';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->images_dir = dirname(__FILE__).'/views/img/banner/';
        $this->displayName = $this->l('OrangePix Checkout Banner');
        $this->description = $this->l('Custom Banner for Checkout Page');

        $this->confirmUninstall = $this->l('Are you sure uninstall this module');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');
        $this->installTemplatesOverrides($this->local_path.'themes_overrides/','tpl');

        // Create image path if not exist
        if (!file_exists($this->images_dir)){
            mkdir($this->images_dir,0777,true);
        }

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayExpressCheckout') &&
            $this->registerHook('displayBannersCheckout') &&
            $this->registerHook('displayPopUp') &&
            $this->registerHook('displayHome');

    }

    public function uninstall()
    {
        //remove images folder
        if (file_exists($this->images_dir)){
            if (PHP_OS === 'Windows')
            {
                exec("rd /s /q {$this->images_dir}");
            }
            else
            {
                exec("rm -rf {$this->images_dir}");
            }
        }
        //Delete Home Text Values
        foreach (Language::getLanguages(false) as $lang){
            Configuration::deleteByName('HOME_TEXT_'.$lang['id_lang']);
        }



        include(dirname(__FILE__).'/sql/uninstall.php');
        $this->rollbackTemplatesOverrides($this->local_path.'themes_overrides/','tpl');

        return parent::uninstall();
    }
    /**
     * Install and override files on proyect folders
     * @param string $directory
     * @param string $ext
     */
    public function installTemplatesOverrides($directory, $ext='php'){
        foreach (Tools::scandir($directory, $ext, '', true) as $file) {
            $name = pathinfo($file)['filename'];
            $original_file = _PS_ROOT_DIR_.'/'.$file;
            if ($this->rename_file($original_file,$name.'.backup',false) || !file_exists($original_file))
                copy($directory.$file,$original_file);
        }
    }


    /**
     * Rollback Installed files on proyect folders
     * @param string $directory
     * @param string $ext
     */
    public function rollbackTemplatesOverrides($directory, $ext='php'){
        foreach (Tools::scandir($directory, $ext, '', true) as $file) {
            $name = pathinfo($file)['filename'];
            $path = pathinfo($file)['dirname'];
            $original_name = pathinfo($file)['basename'];
            $override_file = _PS_ROOT_DIR_.'/'.$file;
            $backup_file= _PS_ROOT_DIR_.'/'.$path.'/'.$name.'.backup';
            if (file_exists($backup_file)){
                unlink($override_file);
                $this->rename_file($backup_file,$original_name,false);
            }
        }
    }

    /**
     * Rename file
     * @param string    $old_path
     * @param string    $name
     * @param bool      $keep_ext Preserve original Extension or not
     *
     * @return bool
     */
    public function rename_file($old_path, $name,$keep_ext=true)
    {
        if (file_exists($old_path)) {
            $info=pathinfo($old_path);
            $new_path=$info['dirname']."/".($keep_ext ? $name.".".$info['extension'] : $name);
            if (file_exists($new_path)) {
                return false;
            }
            return rename($old_path, $new_path);
        }
        return false;
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitCard')) == true) {
            $this->saveNewBannerCard();
        }
        if (((bool)Tools::isSubmit('submit-btn-delete-image')) == true) {
            $this->deleteBannerCard();
        }
        if (((bool)Tools::isSubmit('submit-btn-save-image')) == true) {
            $this->updateBannerCard();
        }
        if (((bool)Tools::isSubmit('submitUpdatePreview')) == true) {
            $this->updateSliderPreviewHTML(true);
        }
        if (((bool)Tools::isSubmit('submitHomeText')) == true) {
            $this->saveHomeText();
        }

        $token = Tools::getAdminTokenLite('AdminModules');
        $action_form = $this->context->link->getAdminLink('AdminModules', false)
          .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.$token;
        $this->context->smarty->assign('action_form', $action_form);
        $this->context->smarty->assign('module_dir', $this->_path);

        $this->context->smarty->assign('slider', $this->updateSliderPreviewHTML());
        $this->context->smarty->assign('banner_list', $this->getBannerListHTML());
        $this->context->smarty->assign('langs', Language::getLanguages(false));
        $this->context->smarty->assign('home_text', $this->getHomeText(true));
        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output;
    }

    /**
     * Save Custom Home Text
     */
    public function saveHomeText(){
        foreach (Language::getLanguages(false) as $lang){
            Configuration::updateValue('HOME_TEXT_'.$lang['id_lang'], Tools::getValue('home-text_'.$lang['id_lang']));
        }
    }

    public function getHomeText($allLanguajes = false){
        if ($allLanguajes){
            $home_texts = array();
            foreach (Language::getLanguages(false) as $lang){
                $home_texts[$lang['id_lang']] = Configuration::get('HOME_TEXT_'.$lang['id_lang']);
            }
            return $home_texts;
        }
        return Configuration::get('HOME_TEXT_'.$this->context->language->id);
    }

    /**
     * Save form data.
     */
    public function saveNewBannerCard()
    {
        $image = Tools::fileAttachment('image-file');
        if (!file_exists($this->images_dir)){
            mkdir($this->images_dir,0777,true);
        }
        move_uploaded_file($image['tmp_name'], $this->images_dir.$image['rename']);
        $data['image_url'] = $this->_path.'views/img/banner/'.$image['rename'];
        $result = Db::getInstance()->insert($this->name,$data,false,false,Db::INSERT,false);
        $id_banner = Db::getInstance()->Insert_ID();
        foreach (Language::getLanguages(false) as $lang){
            $data = array();
            $data['id_banner'] = $id_banner;
            $data['id_lang'] = $lang['id_lang'];
            $data['product_name'] = Tools::getValue('product-name_'.$lang['id_lang']);
            $data['price'] = (float)Tools::getValue('product-price_'.$lang['id_lang']);
            $data['link'] = Tools::getValue('product-link_'.$lang['id_lang']);
            
            $result = Db::getInstance()->insert($this->name.'_lang',$data,false,false,Db::INSERT,false);
        }

        die($this->getBannerListHTML());

    }

    /**
     * Get Banner List from database
     * @return array|false|mysqli_result|PDOStatement|resource|null
     * @throws PrestaShopDatabaseException
     */
    public function getBannerList($where = ''){
        $sql = 'SELECT * FROM '.$this->name.' b INNER JOIN '.$this->name.'_lang l ON b.id_banner = l.id_banner '.($where!=''? 'WHERE '.$where :'').';';
        $result = Db::getInstance()->executeS($sql,array(),false);
//        if (count($result)==1 && $where!='')
//            return $result[0];

        $banners = array();
        foreach ($result as $banner){
            $banners[$banner['id_banner']]['id_banner']= $banner['id_banner'];
            $banners[$banner['id_banner']]['image_url']= $banner['image_url'];
            $banners[$banner['id_banner']][$banner['id_lang']]['id_lang']= $banner['id_lang'];
            $banners[$banner['id_banner']][$banner['id_lang']]['product_name']= $banner['product_name'];
            $banners[$banner['id_banner']][$banner['id_lang']]['price']= $banner['price'];
            $banners[$banner['id_banner']][$banner['id_lang']]['link']= $banner['link'];
        }
        return $banners;
    }

    /**
     * Return Banner list in HTML template
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws SmartyException
     */
    public function getBannerListHTML(){
        $this->context->smarty->assign('banners', $this->getBannerList());
        $this->context->smarty->assign('langs', Language::getLanguages(false));
        $token = Tools::getAdminTokenLite('AdminModules');
        $action_form = $this->context->link->getAdminLink('AdminModules', false)
          .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.$token;
        $this->context->smarty->assign('action_form', $action_form);
        return $this->context->smarty->fetch($this->local_path.'views/templates/admin/banner-list.tpl');
    }

    public function getBannerHTML($where = '',$orderTotal,$horizontal=false){
        $banners = $this->getBannerList($where);
        $html ='';
        if (count($banners)>0){
            foreach ($banners as $banner){
                $amount = (float)$banner[$this->context->language->id]['price'] - (float)$orderTotal;
                $this->context->smarty->assign('banner', $banner);
                $this->context->smarty->assign('lang', $this->context->language->id);
                $this->context->smarty->assign('amount', $amount);
                if ($horizontal){
                    $html .= $this->context->smarty->fetch($this->local_path . 'views/templates/hook/banner-horizontal.tpl');
                }else{
                    $html .= $this->context->smarty->fetch($this->local_path . 'views/templates/hook/banner.tpl');
                }
            }

            try {
                return $html;
            } catch (SmartyException $e) {
            } catch (Exception $e) {
            }
        }

    }

    /**
     * Update Slider Preview on BackOffice
     *
     * @param bool   $update
     * @param string $where
     *
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws SmartyException
     */
    public function updateSliderPreviewHTML($update=false,$where = ''){
        $this->context->smarty->assign('lang', $this->context->language->id);
        $this->context->smarty->assign('banners', $this->getBannerList($where));
        $this->context->smarty->assign('home_text', $this->getHomeText(true));
        $slider = $this->context->smarty->fetch($this->local_path.'views/templates/front/slider.tpl');
        if ($update)
            die($slider);

        return $slider;
    }

    /**
     * Delete Image Card
     */
    public function deleteBannerCard(){
        $id = Tools::getValue('id-banner');
        try {
            $sql = 'SELECT b.image_url FROM '.$this->name.' b  WHERE b.id_banner='.$id.';';
            $image = Db::getInstance()->executeS($sql)[0];
            if (file_exists(_PS_ROOT_DIR_.$image['image_url']))
                unlink(_PS_ROOT_DIR_.$image['image_url']);
        }catch (PrestaShopDatabaseException $exception){}

        $result = Db::getInstance()->delete($this->name,'id_banner='.$id,0,false,false);
        $result = Db::getInstance()->delete($this->name.'_lang','id_banner='.$id,0,false,false);


        try {
            die($this->getBannerListHTML());
        } catch (PrestaShopDatabaseException $e) {
        } catch (SmartyException $e) {
        }
    }

    /**
     * Update Image Card
     */
    public function updateBannerCard(){
        $id = Tools::getValue('id-banner');

        foreach (Language::getLanguages(false) as $lang){
            $data = array();
            $data['product_name'] = Tools::getValue('product-name_'.$lang['id_lang']);
            $data['price'] = (float)Tools::getValue('product-price_'.$lang['id_lang']);
            $data['link'] = Tools::getValue('product-link_'.$lang['id_lang']);

            $result = Db::getInstance()->update($this->name.'_lang',$data,'id_banner='.$id.' AND id_lang='.$lang['id_lang'],0,false,false,false);
        }

        try {
            die($this->getBannerListHTML());
        } catch (PrestaShopDatabaseException $e) {
        } catch (SmartyException $e) {
        }
    }



    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('configure') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addJS($this->_path.'views/js/bs-custom-file-input.min.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {

        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayPopUp($params)
    {

           return $this->renderBanner($params,true);

    }

    public function hookDisplayHome()
    {
        try {
            return $this->updateSliderPreviewHTML(false,'id_lang='.$this->context->language->id);
        } catch (PrestaShopDatabaseException $e) {
        } catch (SmartyException $e) {
        }
    }

    public function hookDisplayBannersCheckout($params)
    {
        //checkout banner horizontal - is not dinamic updated
    }

    public function hookDisplayExpressCheckout($params)
    {
        return $this->renderBanner($params);
    }

    public function renderBanner($params,$horizontal=false){
        try {
            $orderTotal = $params['cart']->getordertotal();
            $html= '<div class="cart-grid row" style="margin-top: 20px;text-align: -webkit-center;">';
            $html .= $this->getBannerHTML('id_lang='.$this->context->language->id.' AND price<='.$orderTotal.' ORDER BY price asc',$orderTotal,$horizontal);
            $html .= $this->getBannerHTML('id_lang='.$this->context->language->id.' AND price>'.$orderTotal.' ORDER BY price asc LIMIT 1',$orderTotal,$horizontal);
            $html .= '</div>';
            return $html;
        } catch (PrestaShopDatabaseException $e) {
        } catch (SmartyException $e) {
        }
    }
}
