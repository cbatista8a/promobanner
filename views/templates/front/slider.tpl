<section id="orangepromomodule">
        <div class="container">
            <div class="row flex-vertical-align  p-y-3">
                {if !empty($home_text)}
                    <div class="col-lg-3">
                            <h2 class="text-white m-t-2">{l s='Rendi il tuo ordine ancora più speciale' mod='op_promobanner'}</h2>
                            <p class="text-white m-b-2">{$home_text[$lang]}</p>
                    </div>
                {/if}
                    <div class="col-lg-9 carosello-promo text-lg-center">
                    {foreach from=$banners item=banner key=key}
                        <div class="slick-item">
                            <a href="{$banner[$lang].link}">
                            <figure>
                            <img src="{$banner.image_url}" alt="{$banner[$lang].product_name}">
                                <figcaption class="caption">
                                    <h5 class="text-white title">{$banner[$lang].product_name}</h5>
                                </figcaption>
                            </figure>
                                <h5 class="text-white m-t-1">{l s='In regalo con un acquisto minimo di:' mod='op_promobanner'}</h5>
                                <p class="text-white m-b-0 p-b-0e">{$banner[$lang].price}€</p>
                            </a>
                        </div>
                    {/foreach}
                    </div>
            </div>
        </div>
</section>