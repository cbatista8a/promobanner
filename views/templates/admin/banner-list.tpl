
    <ul class="list-unstyled  banner-list">
        {foreach from=$banners item=banner key=key}
            <li class="media pt-4 border single-image" data-id="{$banner.id_banner}">
                <div class="arrow-area m-3 d-inline-block">
                    <i class="fas fa-arrows-alt arrow-sorting"></i>
                </div>
                <form class="form-save-banner col row" method="post" action="{$action_form}">
                    <div class="banner-thumbnail col-3 p-3 mr-2">
                        <img class="border" src="{$banner.image_url}" height="250px" width="350px">
                    </div>
                    <div class="media-body col pt-5 form-group">

                        <input type="hidden" name="id-banner" value="{$banner.id_banner}">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="product-name-label">{l s='Product Name' mod='op_promobanner'}</span>
                                {$count=0}
                                {foreach from=$langs item=lang}
                                    <div class="translatable-field col row lang-{$lang.id_lang}" {if $count!=0}style="display:none"{/if}{$count=$count+1}>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="product-name_{$lang.id_lang}" value="{$banner[$lang.id_lang].product_name}" aria-describedby="product-name-label" style="height: auto" required onkeyup="if (isArrowKey(event)) return ;updateFriendlyURL();">
                                        </div>
                                        <div class="col-lg-1">
                                            <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown" aria-expanded="false">
                                                {$lang.iso_code}
                                                <i class="icon-caret-down"></i>
                                            </button>
                                            <ul class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(5px, 31px, 0px); top: 0px; left: 0px; will-change: transform; overflow: visible" >
                                                {foreach from=$langs item=lang}
                                                    <li><a href="javascript:hideOtherLanguage({$lang.id_lang});" tabindex="-1">{$lang.name}</a></li>
                                                {/foreach}
                                            </ul>
                                        </div>
                                    </div>
                                {/foreach}
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="product-price-label">{l s='Price' mod='op_promobanner'}</span>
                                {$count=0}
                                {foreach from=$langs item=lang}
                                    <div class="translatable-field col row lang-{$lang.id_lang}" {if $count!=0}style="display:none"{/if}{$count=$count+1}>
                                        <div class="col-lg-6">
                                            <input type="number" step="any" min="0" class="form-control" name="product-price_{$lang.id_lang}" value="{$banner[$lang.id_lang].price}" aria-describedby="product-price-label" style="height: auto" required onkeyup="if (isArrowKey(event)) return ;updateFriendlyURL();">
                                        </div>
                                        <div class="col-lg-1">
                                            <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown" aria-expanded="false">
                                                {$lang.iso_code}
                                                <i class="icon-caret-down"></i>
                                            </button>
                                            <ul class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(5px, 31px, 0px); top: 0px; left: 0px; will-change: transform; overflow: visible" >
                                                {foreach from=$langs item=lang}
                                                    <li><a href="javascript:hideOtherLanguage({$lang.id_lang});" tabindex="-1">{$lang.name}</a></li>
                                                {/foreach}
                                            </ul>
                                        </div>
                                    </div>
                                {/foreach}
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="product-link-label">{l s='Link' mod='op_promobanner'}</span>
                                {$count=0}
                                {foreach from=$langs item=lang}
                                    <div class="translatable-field col row lang-{$lang.id_lang}" {if $count!=0}style="display:none"{/if}{$count=$count+1}>
                                        <div class="col-lg-6">
                                            <input type="url" class="form-control" name="product-link_{$lang.id_lang}" value="{$banner[$lang.id_lang].link}" style="height: auto" aria-describedby="product-link-label" required onkeyup="if (isArrowKey(event)) return ;updateFriendlyURL();">
                                        </div>
                                        <div class="col-lg-1">
                                            <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown" aria-expanded="false">
                                                {$lang.iso_code}
                                                <i class="icon-caret-down"></i>
                                            </button>
                                            <ul class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(5px, 31px, 0px); top: 0px; left: 0px; will-change: transform; overflow: visible" >
                                                {foreach from=$langs item=lang}
                                                    <li><a href="javascript:hideOtherLanguage({$lang.id_lang});" tabindex="-1">{$lang.name}</a></li>
                                                {/foreach}
                                            </ul>
                                        </div>
                                    </div>
                                {/foreach}
                            </div>
                        </div>
                    </div>
                    <div class="p-5 col-2 form-group" style="height: 250px;">
                        <button type="button" class="btn btn-outline-success btn-block border btn-save-banner" name="btn-save-banner">{l s='Save' mod='op_promobanner'}</button>
                        <button type="button" class="btn btn-outline-danger btn-block border btn-delete-banner" name="btn-delete-banner">{l s='Delete' mod='op_promobanner'}</button>
                    </div>
                </form>
            </li>
        {/foreach}
        {if count($banners) <= 0}
            <span class="caption m-3"><i class="icon icon-file"></i> {l s=' Not Banners found' mod='op_promobanner'}</span>
        {/if}
    </ul>