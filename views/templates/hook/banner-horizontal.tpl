<div class="banner mt-3 cart-grid-body col-xs-12 col-sm-6 col-lg-4 col-xl-3">
            <div class="banner-item text-center">
                <a href="{$banner[$lang].link}" target="_blank" style="text-decoration: none;">
                <img class="img-fluid"  alt="{$banner[$lang].product_name}" src="{$banner.image_url}"  style="width: 100%;">
                <div class="d-md-block" style="text-align: center; margin-top: 10px;">
                    {if $amount<=0}
                        {l s='You are win as a gift: ' mod='op_promobanner'}
                    {else}
                        {l s='With %s€ more you win as a gift: ' sprintf=[$amount] mod='op_promobanner'}
                    {/if}
                    <br/>
                    <span class="d-block">{$banner[$lang].product_name}</span>
                    <span class="d-inline">{l s='Price: ' mod='op_promobanner'}{$banner[$lang].price}€</span>
                </div>
                </a>
            </div>
</div>
